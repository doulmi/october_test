/* global describe, it */
const { expect } = require('chai');
const supertest = require('supertest');
const server = require('../bin/www');

const request = supertest(server);

describe('collection of companies test spec', () => {
  // test cases
  describe('companies show route', () => {
    it('name not exists', () => {
      request.get('/companies/not_exist_name').end((err, response) => {
        expect(response.statusCode).to.equal(404);
      });
    });
    it('uppercase companies', () => {
      request.get('/companies/EXPERDECO').end((err, response) => {
        expect(response.statusCode).to.equal(200);
        expect(response.body).to.eql({
          siren: 303830244,
          name: 'EXPERDECO',
          address: '74970 MARIGNIER',
        });
      });
    });
    it('lowercase companies', () => {
      request.get('/companies/experdeco').end((err, response) => {
        expect(response.statusCode).to.equal(200);
        expect(response.body).to.eql({
          siren: 303830244,
          name: 'EXPERDECO',
          address: '74970 MARIGNIER',
        });
      });
    });
    it('mix case companies', () => {
      request.get('/companies/EXPErdeco').end((err, response) => {
        expect(response.statusCode).to.equal(200);
        expect(response.body).to.eql({
          siren: 303830244,
          name: 'EXPERDECO',
          address: '74970 MARIGNIER',
        });
      });
    });
    it('find with a part fo companie name', () => {
      request.get('/companies/INTERNATIONAL').end((err, response) => {
        expect(response.statusCode).to.equal(200);
        expect(response.body).to.eql({
          siren: 301941407,
          name: 'SA LUBING INTERNATIONAL',
          address: '62840 SAILLY SUR LA LYS',
        });
      });
    });
  });
});
