const companiesData = require('../data/companies');

const getCompanyByName = name => {
  const uppercaseName = name.toUpperCase();
  for (let i = 0; i < companiesData.length; i += 1) {
    if (companiesData[i].name.includes(uppercaseName)) {
      return companiesData[i];
    }
  }
  return null;
};

exports.getCompanyByName = getCompanyByName;
