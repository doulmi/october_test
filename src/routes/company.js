const express = require('express');
const { getCompanyByName } = require('../controllers/companiesController');

const router = express.Router();

const show = (req, res) => {
  const { name } = req.params;
  if (!name || name.length === 0) {
    res.status(422).send('Name can be empty');
    return;
  }
  const company = getCompanyByName(name);
  if (company && company.siren) {
    res.status(200).json(company);
    return;
  }
  res.status(404).send('This company not exists');
};

router.get('/:name', show);

module.exports = router;
