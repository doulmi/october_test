const express = require('express');

const router = express.Router();

const index = (req, res) => {
  res.status(200).send(`<pre>
 ______
&lt; October ! &gt;
 ------
        \\   ^__^
         \\  (oo)\\_______
            (__)\\       )/\\
                ||----w |
                ||     ||
</pre>
`);
};

router.get('/', index);

module.exports = router;
