const express = require('express');
const compression = require('compression');
const indexRouter = require('./routes/index');
const companyRouter = require('./routes/company');
const logger = require('./helpers/logger');

const app = express();
app.disable('x-powered-by');

// Middlewares
app.use(compression());

// Routes
app.use('/companies', companyRouter);
app.use('*', indexRouter);

app.use(function errorHandler(err, req, res, next) { // eslint-disable-line
  // Log error message in our server's console
  logger.error(err.message, err);
  // All HTTP requests must have a response,
  // so let's send back an error with its status code and message
  // If err has no specified error code, set error code to 'Internal Server Error (500)'
  res.status(err.statusCode || 500).send(err.message);
  // Send error to sentry
});

module.exports = app;
